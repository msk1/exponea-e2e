# Running E2E tests

First install Selenium Server locally:

    npm run webdriver-manager update

Then you need to have Selenium Server running:

    npm run webdriver-manager start

Finally, run tests:

    npm run protractor conf.js