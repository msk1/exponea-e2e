var fixtures = require('../fixtures');

// spec.js
describe('App Login', function() {
    it('user should log in successfully', function() {
        browser['ng12Hybrid'] = true;

        browser.get(fixtures.contextUrl + '/#/p/demo/home');

        expect(browser.getTitle()).toEqual('Exponea');

        element(by.name('username')).sendKeys(fixtures.username);
        element(by.name('password')).sendKeys(fixtures.password);

        element(by.css('button[type=submit]')).click();

        expect(browser.getTitle()).toEqual('Home | Exponea');


    });
});