var fixtures = require('../fixtures');
var inputName = element(by.name('name'));
var baseUrl = '/#/p/exponea1/project/dee-solutions/web-layers';
var targetCustomer ='John';
var expectedNumberOfTargetCustomers='0';
// spec.js
describe('Layer creation', function() {
        it('should open new layer page', function() {
            browser.get(fixtures.contextUrl + '/#/p/exponea1/project/dee-solutions/web-layers');
            expect(element(by.xpath('.//h3[.="Direct Execution Layer"]')).isDisplayed()).toBe(true);
            });
        it('should open new layer form and select settings',function() {
            element(by.className('btn-basic btn-submit')).click();
            inputName.sendKeys('layer test');
            element(by.xpath('.//a[.="Settings"]')).click();
            element(by.xpath('//*[@id="banners-schedule"]/div[5]/div/label[2]/input')).click();
        });
        it('should select an attribute for target customer',function(){
            element(by.xpath('.//*[.="Select an attribute"]')).click();
            element(by.name('search')).click();
            element(by.name('search')).sendKeys('registered');
            var enter = browser.actions().sendKeys(protractor.Key.ENTER);
            enter.perform();
            element(by.xpath('.//*[@placeholder="Select a value"]')).click();
            element(by.xpath('.//*[@placeholder="Select a value"]')).sendKeys(targetCustomer);
        });
        it('should get to summary page and check number of affected customers',function(){
            element(by.xpath('//*[@bs-tooltip="\'Go to the launch summary\'"]')).click();
            expect(element(by.xpath('.//li[@ng-if="filterIsValid() && data.customer_filter"]')).getText()).toEqual("Currently you have "+expectedNumberOfTargetCustomers+" customers that match the filter for this web layer");
        });
        it('should post layer to production',function(){
            element(by.xpath('//*[@bs-tooltip="\'Launch this web layer to production\'"]')).click();
            expect(element(by.xpath('.//h3[.="Integrate the web layer into your app"]')).isDisplayed()).toBe(true);
       });
});